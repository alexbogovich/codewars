def less(l, x):
    for i in l:
        if i < x:
            return True
        elif i > x:
            return False
    return False


def pick_peaks(arr):
    pos = []
    perk = []
    for i in range(1, len(arr)):
        if (arr[i] > arr[i - 1] and less(arr[i + 1:], arr[i])):
            pos.append(i)
            perk.append(arr[i])
    return {'perk': perk, 'pos': pos}


print(pick_peaks([3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 2, 2, 1]))
