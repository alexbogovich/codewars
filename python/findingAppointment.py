from datetime import datetime, timedelta

fmt = '%H:%M'
startWDay = datetime.strptime("09:00", fmt)
endWDay = datetime.strptime("19:00", fmt)


def getDatetime(sDT):
    return [datetime.strptime(sDT[0], fmt), datetime.strptime(sDT[1], fmt)]


def getValidFreeTime(meets, duration):
    end = False
    r = []
    sTime = startWDay
    for datePair in meets:
        dateTimePair = getDatetime(datePair)
        if dateTimePair[1] == endWDay: end = True
        if (dateTimePair[0] - sTime) >= duration:
            r.append([sTime, dateTimePair[0]])
        sTime = dateTimePair[1]
    if not end and (endWDay - sTime) >= duration:
        r.append([sTime, endWDay])
    return r


def test(freeTime: list, sced: list, duration):
    t = []
    for scdTime in sced:
        for item in freeTime:
            if item[0] <= scdTime[0] < item[1] and item[1] - scdTime[0] >= duration:
                t.append([scdTime[0], item[1] if item[1] <= scdTime[1] else scdTime[1]])
            elif item[0] <= scdTime[1] <= item[1] and scdTime[1] - item[0] >= duration:
                t.append([scdTime[0], scdTime[1]])
    return t


def get_start_time(schedules, duration):
    time_delta = timedelta(minutes=duration % 60, hours=duration // 60)
    r2 = [[startWDay, endWDay]]
    for sced in schedules:
        free = getValidFreeTime(sced, time_delta)
        r2 = test(r2, free, time_delta)
    if len(r2) == 0: return None
    return r2[0][0].time().strftime('%H:%M')


schedules = [
    [['09:00', '11:30'], ['13:30', '16:00'], ['16:00', '17:30'], ['17:45', '19:00']],
    [['09:15', '12:00'], ['14:00', '16:30'], ['17:00', '17:30']],
    [['11:30', '12:15'], ['15:00', '16:30'], ['17:45', '19:00']]
]

print(get_start_time(schedules, 90))

# r2 = [[startWDay, endWDay]]
# sced = [['11:20', '11:30'], ['13:30', '16:00'], ['16:00', '17:30'], ['17:45', '17:50']]
# sced2 = [['11:00', '11:30'], ['13:40', '16:00'], ['16:00', '17:00']]
# sced3 = [['11:00', '11:30'], ['13:40', '16:00'], ['16:00', '17:00']]
# free = getValidFreeTime(sced, timedelta(minutes=12))
# free2 = getValidFreeTime(sced2, timedelta(minutes=12))
# free3 = getValidFreeTime(sced2, timedelta(minutes=12))
# print(free)
# print(free2)
# r2 = test(r2, free, timedelta(minutes=12))
# print(r2)
# r2 = test(r2, free2, timedelta(minutes=12))
# r2 = test(r2, free3, timedelta(minutes=12))
# print(r2)
