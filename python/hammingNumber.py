from itertools import tee, chain, groupby, islice
from heapq import merge


def raymonds_hamming():
    def deferred_output():
        for i in output:
            yield i

    result, p2, p3, p5 = tee(deferred_output(), 4)
    m2 = (2 * x for x in p2)
    m3 = (3 * x for x in p3)
    m5 = (5 * x for x in p5)
    merged = merge(m2, m3, m5)
    combined = chain([1], merged)
    output = (k for k, g in groupby(combined))

    return result


def hamming(n):
    return next(islice(raymonds_hamming(), n - 1, n))
