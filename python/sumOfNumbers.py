# https://www.codewars.com/kata/beginner-series-number-3-sum-of-numbers/python
def get_sum(a, b):
    return sum(x for x in range(min(a, b), max(a, b) + 1))


# O(1)
def get_sum_celver(a, b):
    return (a + b) * (abs(a - b) + 1) // 2
