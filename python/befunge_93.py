from random import randint


def interpret(code):
    output = ''
    table = [[' ' for x in range(80)] for y in range(25)]
    x, y = 0, 0
    stack = []
    px, py = 1, 0

    for string in code.split('\n'):
        x = 0
        for char in string:
            table[y][x] = str(char)
            x += 1
        y += 1
    print('Table struct:')
    for line in code.split('\n'): print(line)
    x, y = 0, 0
    string_mode = False
    while table[y][x] != '@':
        c = table[y][x]

        print('char:{} x:{} y:{} stack:{} out:{}'.format(c, x, y, str(stack), output))
        if c == '"':
            if (string_mode):
                string_mode = False
            else:
                string_mode = True
        elif string_mode:
            stack.append(ord(c))

        elif c == '>':
            px, py = 1, 0
        elif c == '<':
            px, py = -1, 0
        elif c == '^':
            px, py = 0, -1
        elif c == 'v':
            px, py = 0, 1
        elif c == '_':
            px, py = 1 if len(stack) != 0 and stack.pop() == 0 else -1, 0
        elif c == '|':
            px, py = 0, 1 if len(stack) != 0 and stack.pop() == 0 else -1
        elif c == '#':
            x, y = x + px, y + py
        elif c == '?':
            px, py = randint(-1, 1), randint(-1, 1)


        elif c == '!':
            stack.append(1 if stack.pop() == 0 else 0)
        elif c == ':':
            stack.append(stack[-1] if len(stack) != 0 else 0)
        elif c == '\\':
            if len(stack) > 1:
                stack[-1], stack[-2] = stack[-2], stack[-1]
            else:
                stack.append(0)

        elif c == '$':
            stack = []  # stack.pop()
        elif c == '.':
            stack.reverse()
            output += ''.join(c for c in stack)  # stack.append(stack.pop())
            stack = []
        elif c == ',':
            # stack.reverse()
            # output += ''.join(chr(c) for c in stack)  # stack.append(chr(stack.pop()))
            # stack = []
            output += chr(stack.pop())

        elif c in '0123456789':
            stack.append(int(c))
        elif c in "+-*/%'":
            a = int(stack.pop())
            b = int(stack.pop())
            if c == '+':
                stack.append(a + b)
            elif c == '-':
                stack.append(b - a)
            elif c == '*':
                stack.append(a * b)
            elif c == '/':
                stack.append(b // a if a != 0 else 0)
            elif c == '%':
                stack.append(b % a if a != 0 else 0)
            elif c == "'":
                stack.append(1 if b > a else 0)

        elif c == 'p':
            ty = stack.pop()
            tx = stack.pop()
            table[ty][tx] = chr(stack.pop())
        elif c == 'g':
            ty = stack.pop()
            tx = stack.pop()
            stack.append(ord(chr(stack.pop())))
        #     return output

        x, y = x + px, y + py
    return output


# norm
# print(interpret('64+"!dlroW ,olleH">:#,_@'))

# out of range
# print(interpret('~:1+!#@_,'))

# an integer is required
print(interpret('01->1# +# :# 0# g# ,# :# 5# 8# *# 4# +# -# _@'))
# unknown
# print(interpret('0v\n"<@_ #! #: #,<*2-1*92,*25,+*92*4*55.0'))


# print(interpret('"dlroW olleH" ,,,,,,,,,,, @'))
# print(interpret('v\n\n\n                   v  ,  <\n>    "dlroW olleH" >  :  |\n                         >  $  @'))
# print(interpret('"dlroW olleH">:#,_@'))
