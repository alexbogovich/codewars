#  https://www.codewars.com/kata/517abf86da9663f1d2000003
import re


def to_camel_case(text):
    return ''.join(word.lower() if i == 0 and word[0:1].lower() == word[0:1] else word.title() for i, word in
                   enumerate(re.split(r'[_-]+', text)))


def to_camel_case_best_practice(s):
    return s[0] + s.title().translate({ord('-'): None, ord('_'): None})[1:] if s else s


def to_camel_case_clever(text):
    return text[0] + ''.join([w[0].upper() + w[1:] for w in text.replace("_", "-").split("-")])[1:] if text else ''


# returns "theStealthWarrior"
print(to_camel_case_clever("the-stealth-warrior"))

# returns "TheStealthWarrior"
print(to_camel_case_clever("The_Stealth_Warrior"))
