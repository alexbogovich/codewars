# https://www.codewars.com/kata/54d496788776e49e6b00052f

def primes(n):
    primfac = []
    d = 2
    while d ** 2 <= n:
        while (n % d) == 0:
            primfac.append(d)
            n //= d
        d += 1
    if n > 1:
        primfac.append(n)
    return primfac


def sum_for_list(lst):
    di = {}
    for item in lst:
        fl = set(primes(abs(item)))
        for f in fl:
            di.update([(f, di.get(f, 0) + item)])
    return sorted(([[key, value] for key, value in di.items()]), key=lambda x: x[0])


def sum_for_list_best(lst):
    factors = {i for k in lst for i in xrange(2, abs(k) + 1) if not k % i}
    prime_factors = {i for i in factors if not [j for j in factors - {i} if not i % j]}
    return [[p, sum(e for e in lst if not e % p)] for p in sorted(prime_factors)]
