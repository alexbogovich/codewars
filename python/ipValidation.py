def is_valid_IP(strng):
    s = strng.split('.')
    if len(s) != 4: return False
    print('len pass')
    for w in s:
        if not w.isdigit(): return False
        print('digit pass')
        if (w[0] == '0' and len(w[1:]) > 0) or int(w) not in range(0, 256): return False
        print('val pass')
    return True


def is_valid_IP_good(strng):
    lst = strng.split('.')
    passed = 0
    for sect in lst:
        if sect.isdigit():
            if sect[0] != '0':
                if 0 < int(sect) <= 255:
                    passed += 1
    return passed == 4


import socket


def is_valid_IP_clever(addr):
    try:
        socket.inet_pton(socket.AF_INET, addr)
        return True
    except socket.error:
        return False
