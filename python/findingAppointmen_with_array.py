def get_start_time(schedules, duration):
    free_minutes = [True] * 60 * 24
    for schedule in schedules:
        for meeting in schedule:
            for i in range(to_minute(meeting[0]), to_minute(meeting[1])):
                free_minutes[i] = False
    for time, available in enumerate(free_minutes):
        if 540 <= time <= 1140 - duration and free_minutes[time:time + duration] == [True] * duration:
            return from_minute(time)
    return None


def to_minute(time):
    times = time.split(':')
    return int(times[0]) * 60 + int(times[1])


def from_minute(minute):
    return ':'.join([str(minute // 60).zfill(2), str(minute % 60).zfill(2)])
