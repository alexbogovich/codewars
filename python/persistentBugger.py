# https://www.codewars.com/kata/55bf01e5a717a0d57e0000ec
import operator


def persistence(n):
    i = 0
    while (len(str(n)) > 1):
        n = reduce(lambda x, y: int(x) * int(y), str(n))
        i += 1
    return i


def persistence_best(n):
    i = 0
    while n >= 10:
        n = reduce(operator.mul, [int(x) for x in str(n)], 1)
        i += 1
    return i
