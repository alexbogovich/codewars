#   https://www.codewars.com/kata/554ca54ffa7d91b236000023
def delete_nth(order, max_e):
    l = list(reversed(order))
    for item in l:
        while l.count(item) > max_e:
            l.pop(l.index(item))
    return list(reversed(l))


def delete_nth_best(order, max_e):
    ans = []
    for o in order:
        if ans.count(o) < max_e: ans.append(o)
    return ans
