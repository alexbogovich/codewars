import random
import re


class Filed(object):
    def __init__(self, code):
        super(Filed, self).__init__()
        self.max_y = 25
        self.max_x = 80
        self.table = []
        self.read_mode = False
        for line in code:
            self.table.append(list(line + ' ' * (self.max_x - len(line))))
            if self.max_y > len(code):
                for i in range(self.max_y - len(code)):
                    self.table.append(list(' ' * self.max_x))
        self.direction = (1, 0)
        self.xy = (0, 0)

    def step(self):
        self.xy = ((self.xy[0] + self.direction[0]) % self.max_x, (self.xy[1] + self.direction[1]) % self.max_y)

    def chg_direction(self, new_direction):
        print('debug:chg_direction {}'.format(new_direction))
        if new_direction == 'up':
            self.direction = (0, -1)
        elif new_direction == 'down':
            self.direction = (0, 1)
        elif new_direction == 'left':
            self.direction = (-1, 0)
        elif new_direction == 'right':
            self.direction = (1, 0)

    def print_field(self):
        for line in self.table:
            if not line == list(' ' * self.max_x):
                print(line)

    def current_char(self):
        return self.table[self.xy[1] % self.max_y][self.xy[0] % self.max_x]

    def get_char(self, x, y):
        return self.table[y % self.max_y][x % self.max_x]

    def put_char(self, y, x, v):  # <>
        self.table[y % self.max_y][x % self.max_x] = chr(v)


def pop(stack):
    return 0 if not stack else stack.pop()


def interpret(code):
    the_field = Filed(code.split('\n'))
    the_field.print_field()
    stack = []
    output = ""
    ops = {"+": lambda x1, x2: stack.append(x1 + x2),
           "-": lambda x1, x2: stack.append(x2 - x1),
           "*": lambda x1, x2: stack.append(x1 * x2),
           "/": lambda x1, x2: stack.append(x2 // x1 if x1 != 0 else 0),
           "%": lambda x1, x2: stack.append(x2 % x1 if x1 != 0 else 0),
           "`": lambda x1, x2: stack.append(1 if x2 > x1 else 0),
           "\\": lambda x1, x2: stack.extend([x1, x2]),
           "g": lambda x1, x2: stack.append(ord(the_field.get_char(x2, x1)))}

    while the_field.current_char() != '@' or the_field.read_mode:
        print('debug:rm={} char={} xy={} stack={} out={}'.format(1 if the_field.read_mode else 0,
                                                                 the_field.current_char(), the_field.xy, stack, output))
        if the_field.current_char() == '\"':
            if the_field.read_mode:
                the_field.read_mode = False
            else:
                the_field.read_mode = True
        elif the_field.read_mode:
            stack.append(ord(the_field.current_char()))
        elif the_field.current_char() == '>':
            the_field.chg_direction('right')
        elif the_field.current_char() == '<':
            the_field.chg_direction('left')
        elif the_field.current_char() == '^':
            the_field.chg_direction('up')
        elif the_field.current_char() == 'v':
            the_field.chg_direction('down')
        elif the_field.current_char() == '?':
            the_field.chg_direction(random.choice(['right', 'left', 'up', 'down']))
        elif re.match("[0-9]", the_field.current_char()):
            stack.append(int(the_field.current_char()))
        elif the_field.current_char() in ops:
            ops[the_field.current_char()](pop(stack), pop(stack))
        elif the_field.current_char() == '!':
            stack.append(1 if pop(stack) == 0 else 0)
        elif the_field.current_char() == '$':
            pop(stack)
        elif the_field.current_char() == '_':
            the_field.chg_direction('right') if pop(stack) == 0 else the_field.chg_direction('left')
        elif the_field.current_char() == '|':
            the_field.chg_direction('down') if pop(stack) == 0 else the_field.chg_direction('up')
        elif the_field.current_char() == '.':
            output += str(pop(stack))
        elif the_field.current_char() == ',':
            output += chr(pop(stack))
        elif the_field.current_char() == '#':
            the_field.step()
        elif the_field.current_char() == ':':
            t = pop(stack)
            stack.extend([t, t])
        elif the_field.current_char() == 'p':
            the_field.put_char(pop(stack), pop(stack), pop(stack))
        the_field.step()
    return output


# test 1 "123456789"
# interpret('>987v>.v\nv456<  :\n>321 ^ _@')
# test 2 "Hello, World!"
# interpret('64+"!dlroW ,olleH">:#,_@')
# test 3
# TODO: Происходит зацикливание в 3м тесте.
interpret(
    '2>:3g" "-!v\  g30          <\n |!`"O":+1_:.:03p>03g+:"O"`|\n @               ^  p3\" ":<\n2 234567890123456789012345678901234567890123456789012345678901234567890123456789')
# done
# @ End program.
# 0-9 Push this number onto the stack. => str(cc)
# > Start moving right.
# < Start moving left.
# ^ Start moving up.
# v Start moving down.
# _ Pop a value; move right if value = 0, left otherwise.
# : Duplicate value on top of the stack. If there is nothing on top of the stack, push a 0.
# . Pop value and output as an integer. => str(pop())

# done, but need to check
# + Addition: Pop a and b, then push a+b.
# - Subtraction: Pop a and b, then push b-a.
# * Multiplication: Pop a and b, then push a*b.
# / Integer division: Pop a and b, then push b/a, rounded down. If a is zero, push zero.
# % Modulo: Pop a and b, then push the b%a. If a is zero, push zero.
# ! Logical NOT: Pop a value. If the value is zero, push 1; otherwise, push zero.
# ` Greater than: Pop a and b, then push 1 if b>a, otherwise push zero.
# ? Start moving in a random cardinal direction.
# | Pop a value; move down if value = 0, up otherwise.
# \ Swap two values on top of the stack. If there is only one value, pretend there is an extra 0 on bottom of the stack.
# $ Pop value from the stack and discard it.
# , Pop value and output the ASCII character represented by the integer code that is stored in the value. => chr(pop())
# # Trampoline: Skip next cell.
# (i.e. a space) No-op. Does nothing.
# " Start string mode: push each character's ASCII value all the way up to the next ". => str(cc)
# g A "get" call (a way to retrieve data in storage). Pop y and x, then push ASCII value of the character at that position in the program.
# p A "put" call (a way to store a value for later use). Pop y, x and v, then change the character at the position (x,y) in the program to the character with ASCII value v.
