# https://www.codewars.com/kata/55e2adece53b4cdcb900006c
def race(v1, v2, g):
    if v1 >= v2:
        return None
    t = g / (v2 - v1)
    h = int(round(t, 6));
    t -= h;
    m = int(round(t * 60, 6))
    t -= m / 60
    s = int(round(t * 360 * 10, 6))
    return [h, m, s]


def race_best(v1, v2, g):
    if v1 > v2: return None
    res = g * 3600 / (v2 - v1)
    return [res / 3600, res % 3600 / 60, res % 60]
