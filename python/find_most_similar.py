import difflib


class Dictionary:
    def __init__(self, words):
        self.words = words

    def find_most_similar(self, term):
        return difflib.get_close_matches(term, self.words)[0]


words = ['cherry', 'peach', 'pineapple', 'melon', 'strawberry', 'raspberry', 'apple', 'coconut', 'banana']
test_dict = Dictionary(words)

print(test_dict.find_most_similar('strawbery'))
print(test_dict.find_most_similar('berry'))
print(test_dict.find_most_similar('aple'))
