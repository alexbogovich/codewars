# https://www.codewars.com/kata/541c8630095125aba6000c00

def digital_root(n):
    s = str(n)
    while len(str(s)) > 1:
        s = str(sum(int(x) for x in s))
    return int(s)


def digital_root_best_practice(n):
    return n if n < 10 else digital_root(sum(map(int, str(n))))


def digital_root_clever(n):
    return n % 9 or n and 9
