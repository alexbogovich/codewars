# https://www.codewars.com/kata/55c6126177c9441a570000cc
def order_weight(strng):
    return ' '.join(w for w in sorted(strng.split(), key=lambda x: (sum(int(c) for c in x), x)))
