def square_digits(num):
    r = ''
    for i in str(num):
        r += str(pow(int(i), 2))
    return int(r)


def square_digit_clever(num):
    return int(''.join(str(int(d) ** 2) for d in str(num)))
