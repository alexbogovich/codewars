# https://www.codewars.com/kata/544aed4c4a30184e960010f4
def divisors(integer):
    l = list(x for x in range(2, int(integer / 2 + 1)) if integer % x == 0)
    if len(l) == 0:
        return str(integer) + " is prime"
    else:
        return l


def divisors_clever(n):
    return [i for i in xrange(2, n) if not n % i] or '%d is prime' % n


def divisors_best_practice(num):
    l = [a for a in range(2, num) if num % a == 0]
    if len(l) == 0:
        return str(num) + " is prime"
    return l
