# https://www.codewars.com/kata/valid-parentheses/python
def valid_parentheses(string):
    i = 0
    for c in string:
        if c == ")":
            i -= 1
        elif c == "(":
            i += 1
        if i < 0: return False
    if i > 0: return False
    return True


def valid_parentheses_short(string):
    cnt = 0
    for char in string:
        if char == '(': cnt += 1
        if char == ')': cnt -= 1
        if cnt < 0: return False
    return True if cnt == 0 else False
