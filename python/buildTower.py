#   https://www.codewars.com/kata/576757b1df89ecf5bd00073b
def tower_builder(n_floors):
    i = 0
    l = []
    while i < n_floors:
        j = 1
        s = '*'
        while j < n_floors:
            if j <= i:
                s = '*' + s + '*'
            else:
                s = ' ' + s + ' '
            j += 1
        i += 1
        l.append(s)
    return l


def tower_builder_best(n):
    return [("*" * (i * 2 - 1)).center(n * 2 - 1) for i in range(1, n + 1)]
