# https://www.codewars.com/kata/youre-a-square/python

import math


def is_square(n):
    if n < 0: return False
    return (int(math.sqrt(n))) ** 2 == n


def is_square(n):
    return n > 0 and math.sqrt(n).is_integer()
